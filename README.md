# Docker images of Coq with primitive-floats and the posdef_check tactic

This repo gathers the recipes to build Docker images gathering
[Coq with primitive-floats](https://github.com/coq/coq/pull/9867)
and the
[posdef_check tactic](https://github.com/validsdp/validsdp/blob/posdef_check/theories/posdef_check.v)
with its dependencies, which are stored in the associated
[GitLab CI registry](https://gitlab.com/erikmd/docker-coq-primitive-floats/container_registry).

These images are useful to run
[benchmarks for primitive-floats in Coq](https://github.com/validsdp/benchs-primitive-floats#readme).

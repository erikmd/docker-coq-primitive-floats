ARG COQ_VERSION="8.15"
FROM coqorg/coq:${COQ_VERSION}-native-flambda

ARG MATHCOMP_VERSION="dev"
ARG INTERVAL_VERSION="dev"
ARG VALIDSDP_VERSION="dev"
ARG VALIDSDP_COMMIT

WORKDIR /home/coq

SHELL ["/bin/bash", "--login", "-c"]

RUN set -x \
  && opam config list && opam repo list && opam list \
  && opam update -y \
  && opam pin add -n -y -k version coq-mathcomp-ssreflect "${MATHCOMP_VERSION}" \
  && opam pin add -n -y -k version coq-interval "${INTERVAL_VERSION}" \
  && ulimit -s \
  && ulimit -s 32768 \
  && ulimit -s \
  && opam install -y -v -j ${NJOBS} coq-interval \
  && sudo apt-get update -y -q \
  && if [ -n "${VALIDSDP_COMMIT}" ]; then opam pin add -n -y -k git "coq-libvalidsdp.${VALIDSDP_VERSION}" "git+https://github.com/validsdp/validsdp.git#${VALIDSDP_COMMIT}" && opam pin add -n -y -k git "coq-validsdp.${VALIDSDP_VERSION}" "git+https://github.com/validsdp/validsdp.git#${VALIDSDP_COMMIT}"; else opam pin add -n -y -k version coq-libvalidsdp "${VALIDSDP_VERSION}" && opam pin add -n -y -k version coq-validsdp "${VALIDSDP_VERSION}"; fi \
  && opam install --confirm-level=unsafe-yes -v -j ${NJOBS} coq-validsdp \
  && opam clean -a -c -s --logs \
  && sudo apt-get clean && sudo rm -rf /var/lib/apt/lists/*

COPY --chown=coq:coq .emacs .emacs

# Install GNU Emacs (tty)
RUN sudo apt-get update -y -q \
  && DEBIAN_FRONTEND=noninteractive sudo apt-get install -y -q --no-install-recommends \
    ca-certificates \
    curl \
    emacs-nox \
    locales \
  && sudo sed -i -e 's/# \(en_US\.UTF-8 .*\)/\1/' /etc/locale.gen \
  && sudo locale-gen \
  && sudo update-locale LANG=en_US.UTF-8 \
  && sudo apt-get clean \
  && sudo rm -rf /var/lib/apt/lists/* \
  && emacs --batch -l "${HOME}/.emacs"

ENV LANG en_US.UTF-8
# ENV LC_ALL en_US.UTF-8
ENV LANGUAGE en_US:en

CMD ["/bin/bash"]
